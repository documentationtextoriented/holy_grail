# Holy grail of documentation generation

## Unix history

![Unix](generated/unix.svg)

## Pets adopted by volunteers

Pets:

- Dogs
- Cats
- Rats

| Pets  |  number |
|-------|---------|
| Dogs  | 386     |
| Cats  | 85      |
| Rats  | 15      |


![Diagram](generated/pie.png)


## Classe

![Classes](generated/classe.png)
